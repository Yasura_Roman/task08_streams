package com.romanyasiura;

import com.romanyasiura.view.View;

public class Application {
    public static void main(String[] args) {
        View view = new View();
        view.show();
    }
}
