package com.romanyasiura.stream.task3;

import com.romanyasiura.view.Printable;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Task3 implements Printable {

    private Logger logger;

    public Task3(Logger logger) {
        this.logger = logger;
    }

    public List<Integer> streamGenerate(int size) {
        return Stream.generate(new Random()::nextInt)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Integer> streamIterate(int size) {
        Random random = new Random();
        int start= random.nextInt();
        int step = random.nextInt();
        return Stream.iterate(start, i -> i + step)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Integer> streamRange(int size) {
        Random random = new Random();
        int start = random.nextInt();
        int stop = random.nextInt();
        return IntStream.range(start, stop)
                .limit(size)
                .boxed()
                .collect(Collectors.toList());
    }

    public void streamStatistic(List<Integer> values, Logger logger) {
        String valuesString = values.stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(", "));
        logger.info("List values : " + valuesString);

        long count = values.stream().count();
        logger.info("count = " + count);

        OptionalDouble average = values.stream().mapToInt(e -> e).average();
        logger.info("average = " + average.getAsDouble());

        OptionalInt min = values.stream().mapToInt(e->e).min();
        logger.info("min = " + min.getAsInt());

        int max = values.stream().max(Integer::compareTo).get();
        logger.info("max = " + max);

        int sum = values.stream().mapToInt(i -> i).sum();
        logger.info("sum = " + sum);

        int reduceSum = values.stream().reduce(0,(a, b) -> a + b);
        logger.info("reduceSum = " + reduceSum);

        int countValuesBiggerThanAverage;
        values.stream().filter( e -> e>average.getAsDouble()).count();
    }


    @Override
    public void print() {
        List<Integer> list = Arrays.asList(new Integer[]{ 22, 12, 234, 4, 44, 44,213, 65, 77, 5});
        streamStatistic(list,logger);
    }
}
