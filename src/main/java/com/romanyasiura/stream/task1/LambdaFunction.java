package com.romanyasiura.stream.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Stream;

public class LambdaFunction {

    private  int a = 8;
    private int b = 15;
    private int c = 2;

    public void task1(Logger logger){

        MyFunctionalInterface firstLambda = (a,b,c) -> {
            Stream<Integer> stream = Arrays.stream(new Integer[]{a,b,c});
            OptionalInt max = stream.mapToInt(e -> e).max();
            return max.getAsInt();
        };

        MyFunctionalInterface secondLambda  = (a,b,c) -> {
            Stream<Integer> stream = Arrays.stream(new Integer[]{a,b,c});
            OptionalDouble average = stream.mapToInt(e -> e).average();
            return (int) Math.round(average.getAsDouble());
        };

        logger.info("Max of " + a + " " + b + " " + c+ ";");
        logger.info(firstLambda.function(a, b, c));
        logger.info("Average  of " + a + " " + b + " " + c+ ";");
        logger.info(secondLambda.function(a, b, c));
    }

    public static void main(String[] args) {
        new LambdaFunction().task1(LogManager.getLogger());
    }
}
