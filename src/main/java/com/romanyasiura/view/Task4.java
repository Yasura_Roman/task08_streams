package com.romanyasiura.view;

import org.apache.logging.log4j.Logger;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Task4 {
    private Logger logger;
    private Scanner scanner;

    public Task4(Logger logger, Scanner scanner) {
        this.logger = logger;
        this.scanner = scanner;
    }

    public void show() {
        logger.info("Hello, please enter text(ends with empty line)");
        String text = "";
        try {
            while (true) {
                String line = scanner.nextLine();
                if (line.equals("")) {
                    break;
                }
                text += " " + line;
            }
            printStatistic(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void printStatistic(String inText) {

        String text = inText.trim();

        List<String> uniqueWords = Arrays.asList(text.split(" "))
                .stream()
                //.map(e -> e.toLowerCase())
                .distinct()
                .collect(Collectors.toList());
        logger.info("Unique words: ");
        uniqueWords.forEach(w -> {
            logger.info(w);
        });

        long uniqueWordsCount = uniqueWords
                .stream()
                .count();
        logger.info("Unique words count: " + uniqueWordsCount);

        List<String> sortedUniqueWords = uniqueWords.stream()
                .sorted(String::compareTo)
                .collect(Collectors.toList());
        logger.info("Sorted unique words: ");
        sortedUniqueWords.forEach(w -> {
            logger.info(w);
        });

        long wordsCount = Arrays.asList(text.split(" "))
                .stream()
                .count();
        logger.info("Words count: " + wordsCount);

        Map<String, Long> allWordsCount = Arrays.asList(text.split(" "))
                .stream()
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        logger.info("Words frequency:");
        allWordsCount.forEach((k, v) -> {
            logger.info(k + " - " + v);
        });

        Map<Character, Long> letterCount = Arrays.asList(text.split(" "))
                .stream()
                .flatMapToInt(s -> s.chars())
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        logger.info("letter frequency:");
        letterCount.forEach((k, v) -> {
            logger.info(k + " - " + v);
        });
    }
}
