package com.romanyasiura.view;

public interface Printable {
    void print();
}