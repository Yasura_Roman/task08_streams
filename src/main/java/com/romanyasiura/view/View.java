package com.romanyasiura.view;

import com.romanyasiura.stream.task1.LambdaFunction;
import com.romanyasiura.stream.task3.Task3;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();

    public View() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Task 1");
        menu.put("2", "  2 - Task 3");
        menu.put("3", "  3 - Task 4");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", () -> new LambdaFunction().task1(logger));
        methodsMenu.put("2", new Task3(logger));
        methodsMenu.put("3", this::executeTask4);
        methodsMenu.put("Q", new Printable() {
            @Override
            public void print() {
                System.exit(0);
            }
        });
    }

    private void executeTask4() {
        new Task4(logger,input).show();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (true);
    }
}

